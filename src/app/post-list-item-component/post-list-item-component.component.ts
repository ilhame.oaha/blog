import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.css']
})
export class PostListItemComponentComponent implements OnInit {

  @Input () postItems;
  
  constructor() { 
    
  }

  ngOnInit() {
  }

  loveit(){
   return  this.postItems.loveIts++;
  
  }
  Dontloveit(){
   return this.postItems.loveIts--;
 
  }
}
