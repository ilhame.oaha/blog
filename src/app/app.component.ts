import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident  sunt in culpa qui officia deserunt mollit anim id est laborum.";
  created_at= new Date();

  posts = [
    {
      title :'Mon premier post',
      content:this.description,
      loveIts: 0,
      created_at: this.created_at
    },

    {
      title :'Mon deuxieme post',
      content:this.description,
      loveIts: 0,
      created_at: this.created_at
    },
    
    {
      title :'Encore un post',
      content:this.description,
      loveIts: 0,
      created_at: this.created_at
    }
  ];
}
